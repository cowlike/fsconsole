module UnitTest

open NUnit.Framework
open Swensen.Unquote
open Main

[<Test>]
let ``One plus one equals two``() =
    add 1 1 =! 2 + 0 + 0
